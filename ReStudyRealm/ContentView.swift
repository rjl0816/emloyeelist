//
//  ContentView.swift
//  ReStudyRealm
//
//  Created by Lotino Ron Jefferson on 2020/08/28.
//  Copyright © 2020 Lotino Ron Jefferson. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
